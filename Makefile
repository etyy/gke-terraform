all: fmt init validate plan

fmt:
	terraform fmt

init:
	terraform init

validate:
	terraform validate

plan:
	terraform plan

apply:
	terraform apply

destroy:
	terraform destroy

kubecongif:
	gcloud container clusters get-credentials testing --zone europe-west1-b

token:
	@kubectl apply -f kube/gitlab-service-account.yml
	@kubectl get secrets $$(kubectl get serviceaccounts -n kube-system gitlab -o json | jq '.secrets[0] | .name' -r) -n kube-system -o json | jq .data.token -r | base64 -d | pbcopy

zone:
	gcloud compute zones list

syntax:
	pre-commit run
