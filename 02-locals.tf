locals {
  region               = "europe-west1"
  org_id               = "96265498530"
  billing_account      = "01A76A-297C1D-3D8781"
  host_project_name    = "network-dbcare"
  service_project_name = "k8s-dbcare"
  host_project_id      = "${local.host_project_name}-${random_integer.int.result}"
  service_project_id   = "${local.service_project_name}-${random_integer.int.result}"
  projects_api         = "container.googleapis.com"
  secondary_ip_ranges = {
    "pod-ip-range"      = "10.0.0.0/14",
    "services-ip-range" = "10.4.0.0/19"
  }
}
